﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkCodeFirst.Models;

namespace EntityFrameworkCodeFirst
{
    public static class AppData
    {
        public static void Set()
        {
            // Set the |DataDirectory| path used in connection strings to point to the correct directory for console app and migrations
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string relative = @"..\..\App_Data\";
            string absolute = Path.GetFullPath(Path.Combine(baseDirectory, relative));
            AppDomain.CurrentDomain.SetData("DataDirectory", absolute);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            AppData.Set();
            using (var ctx = new ApplicationDbContext())
            {
                var s1 = new Student() { StudentID = 1, StudentName = "XYZ" };
                var s2 = new Student() { StudentID = 2, StudentName = "ABC" };
                var s3 = new Student() { StudentID = 3, StudentName = "LMN" };

                ctx.Students.Add(s1);
                ctx.Students.Add(s2);
                ctx.Students.Add(s3);

                ctx.SaveChanges();

                var students = (from s in ctx.Students
                                orderby s.StudentName
                                select s).ToList();

                foreach (var student in students)
                {
                    Console.WriteLine("ID: {0}, Name: {1}", student.StudentID, student.StudentName);
                }

                Console.ReadKey();
            }
        }
    }
}
